import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 1000;
    private static final int CANVAS_HEIGHT = 600;
    private static final int CANVAS_WIDTH = 600;
    private static int TOTAL_PIXELS;
    private static ImageWrapper imageWrapper;
    private static final JFrame window = new JFrame("Color coverage");
    private static final JPanel results = new JPanel();
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public static void main(String[] args) throws IOException {
        window.setLayout(new FlowLayout(FlowLayout.CENTER));

        // buttons
        JPanel menuButton = new JPanel();
        window.add(menuButton);

        JButton openFileBtn = new JButton("Open file");
        menuButton.add(openFileBtn);

        JButton calculateColorCoverageButton = new JButton("Calculate color ranges");
        menuButton.add(calculateColorCoverageButton);

        // image
        imageWrapper = new ImageWrapper();
        window.add(imageWrapper);

        // file chooser
        final JFileChooser fc = new JFileChooser();
        fc.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("JPEG files", "jpg");
        fc.addChoosableFileFilter(filter);


        openFileBtn.addActionListener(
            e -> {
                readFile(window, fc);
                window.repaint();
            });

        window.add(results);
        PixelCounter counter = new PixelCounter();
        ArrayList<PixelCounter.RGBRange> ranges = new ArrayList<PixelCounter.RGBRange>();
        ranges.add(counter.rgb(175,30,30,60));
        ranges.add(counter.rgb(75,150,10,60));
        ranges.add(counter.rgb(0,0,255,60));
        ranges.add(counter.rgb(255,255,255,60));
        ranges.add(counter.rgb(0,0,0,60));
        calculateColorCoverageButton.addActionListener(
            e -> {
                printResults(counter.count(imageWrapper.getImage(),
                    ranges.toArray(new PixelCounter.RGBRange[0])
                ), ranges.toArray(new PixelCounter.RGBRange[0]));
                window.repaint();
            });



        menuButton.setPreferredSize(new Dimension(WIDTH, 100));
        menuButton.setSize(new Dimension(WIDTH, 100));
        menuButton.setLayout(new GridLayout(1, 2));

        imageWrapper.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
        imageWrapper.setSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));

        window.setSize(new Dimension(WIDTH, HEIGHT));
        window.setVisible(true);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private static void printResults(int[] count, PixelCounter.RGBRange[] ranges) {
        results.removeAll();
        results.setLayout(new GridLayout(ranges.length, 1));
        results.setSize(new Dimension(WIDTH, 200));
        for(int i = 0; i < ranges.length; i++){
            JPanel result = new JPanel();
            JPanel color = new JPanel();
            color.setSize(new Dimension(20, 20));
            color.setBackground(new Color(
                (ranges[i].minR + ranges[i].maxR) / 2,
                (ranges[i].minG + ranges[i].maxG) / 2,
                (ranges[i].minB + ranges[i].maxB) / 2)
            );
            JLabel newField = new JLabel(
                "R: " + ((ranges[i].minR + ranges[i].maxR) / 2) + " " +
                "G: " + ((ranges[i].minG + ranges[i].maxG) / 2) + " " +
                "B: " + ((ranges[i].minB + ranges[i].maxB) / 2) + " " +
                "coverage: " + count[i] + " / " + count[count.length - 1] + " -> " + df.format(((float)count[i] / count[count.length - 1] * 100)) + "%"
            );
            result.add(color);
            result.add(newField);
            results.add(result);
        }
        results.repaint();
    }

    private static void readFile(JFrame window, JFileChooser fc) {
        int returnVal = fc.showOpenDialog(window);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            BufferedImage image = ImageUtils.readFromFile(file);
            TOTAL_PIXELS = image.getHeight() * image.getWidth();
            imageWrapper.setImage(image);
            imageWrapper.repaint();
        }
    }
}
