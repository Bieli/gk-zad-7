import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class PixelCounter {
    public int[] count(BufferedImage image, RGBRange... ranges){
        int[] result = new int[ranges.length+1];
        int h = image.getHeight();
        int w = image.getWidth();
        result[ranges.length] = h * w;
        int[] rgbArray = null;
        for (int y = 0; y < h; y++){
            rgbArray = image.getRGB(0, y, w, 1, rgbArray, 0, w);
            for (int r = 0; r<ranges.length; r++){
                result[r] += ranges[r].count(rgbArray);
            }
        }
        return result;
    }


    public static class RGBRange{
        public final int minR, minG, minB, maxR, maxG, maxB;
        RGBRange(int minR, int minG, int minB, int maxR, int maxG, int maxB){
            // handle possibly reversed ranges
            this.minR = Math.min(minR, maxR);
            this.minG = Math.min(minG, maxG);
            this.minB = Math.min(minB, maxB);
            this.maxR = Math.max(minR, maxR);
            this.maxG = Math.max(minG, maxG);
            this.maxB = Math.max(minB, maxB);
        }

        int count(int[] rgbArray){
            int count = 0;
            for (int pixel: rgbArray){
                // see Color#getRed
                int r = (pixel >> 16) & 0xFF;
                if (r < minR || r > maxR) continue;
                int g = (pixel >> 8) & 0xFF;
                if (g < minG || g > maxG) continue;
                int b = (pixel) & 0xFF;
                if (b < minB || b > maxB) continue;
                count++;
            }
            return count;
        }
    }


    public RGBRange rgb(int minR, int minG, int minB, int maxR, int maxG, int maxB){
        return new RGBRange(minR, minG, minB, maxR, maxG, maxB);
    }


    public RGBRange rgb(int r, int g, int b, int maxDelta){
        return new RGBRange(r-maxDelta, g-maxDelta, b-maxDelta,r+maxDelta, g+maxDelta, b+maxDelta);
    }



}